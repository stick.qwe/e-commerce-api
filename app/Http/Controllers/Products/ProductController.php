<?php

namespace App\Http\Controllers\Products;

use App\Http\Controllers\Controller;
use App\Http\Resources\ProductIndexResource;
use App\Http\Resources\ProductResource;
use App\Models\Product;
use App\Scoping\Scopes\CategoryScope;
use Illuminate\Http\Request;

/**
 * Class ProductController
 * @package App\Http\Controllers\Products
 */
class ProductController extends Controller
{
    /**
     * @return \Illuminate\Http\Resources\Json\AnonymousResourceCollection
     */
    public function index(Request $request)
    {
        $products = Product::with(['variations'])
            ->withScopes($this->scopes())
            ->paginate($request->input('count', 10));

        return ProductIndexResource::collection($products);
    }

    /**
     * @param Product $product
     * @return ProductResource
     */
    public function show(Product $product)
    {

        $product->load(['variations.type', 'variations.stock', 'variations.product']);

        return ProductResource::make($product);
    }

    /**
     * @return CategoryScope[]
     */
    protected function scopes()
    {
        return [
            'category' => new CategoryScope()
        ];
    }
}

<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
 * auth route
 * */
Route::middleware('auth:api')
    ->get('user', fn(Request $request) => $request->user());

// category
Route::resource('categories', 'Categories\CategoryController');
// product
Route::resource('products', 'Products\ProductController');

Route::resource('cart', 'Cart\CartController');
